reset

set terminal epslatex color

set key left

T(x)=0.219+20.456*x-0.302*x**2+0.009*x**3+273.15
f(x)=a*x+b
g(x)=c*x+d

#fit relevant data
#set fit logfile 'fitwarmal.log'
#fit f(x) 'datwarm.dat' u ($10*60):(T($4)) via a,b
#set fit logfile 'fitwarmalab.log'
#fit g(x) 'warmalab.dat' u (($3+4)*60):(log(T($1)-T(0.96))) via c,d


#set fit logfile 'fitwarmfe.log'
#fit f(x) 'fewarm.dat' u ($2*60):(T($1)) via a,b
#set fit logfile 'fitwarmfeab.log'
#fit g(x) 'datwarm.dat' u (($3+11)*60):(log(T($2)-T(0.93))) via c,d


#set fit logfile 'fitkaltal.log'
#fit f(x) 'datkalt.dat' u ($1*60):(T(-$2)) via a,b
#set fit logfile 'fitkaltalab.log'
#fit g(x) 'datkalt.dat' u (($8+15)*60):(log(T(-1*$4)-T(-7.36))) via c,d


#set fit logfile 'fitkaltfe.log'
#fit f(x) 'datkalt.dat' u ($1*60):(T(-$3)) via a,b
#set fit logfile 'fitkaltfeab.log'
#fit g(x) 'datkalt.dat' u (($8+15)*60):(log(T(-$5)-T(-6.68))) via c,d


set output 'tempzeitraum.tex'

set xlabel 'Zeit t [s]'
set ylabel 'Temperatur T [\si{\celsius}]'

p  'datwarm.dat' u ($10*60):(0.219+20.456*$4-0.302*$4**2+0.009*$4**3):(0.01*(20.456-2*0.302*$4+3*0.009*$4**2)) w e t 'AL', 'fewarm.dat' u ($2*60):(0.219+20.456*$1-0.302*$1**2+0.009*$1**3):(0.01*(20.456-2*0.302*$1+3*0.009*$1**2)) w e t 'FE', 'warmalab.dat' u (($3+4)*60):(0.219+20.456*$1-0.302*$1**2+0.009*$1**3):(0.01*(20.456-2*0.302*$1+3*0.009*$1**2)) w e lt 1 notitle, 'warmalab.dat' u (($3+11)*60):(0.219+20.456*$2-0.302*$2**2+0.009*$2**3):(0.01*(20.456-2*0.302*$2+3*0.009*$2**2)) w e lt 2 notitle

set output
!epstopdf tempzeitraum.eps


set output 'tempzeitstick.tex'

p 'datkalt.dat' u ($1*60):(-0.219-20.456*$2+0.302*$2**2-0.009*$2**3):(0.01*(-20.456+2*0.302*$2-3*0.009*$2**2)) w e t 'AL', 'datkalt.dat' u ($1*60):(-0.219-20.456*$3+0.302*$3**2-0.009*$3**3):(0.01*(-20.456+2*0.302*$3-3*0.009*$3**2)) w e t 'FE', 'datkalt.dat' u (($8+15)*60):(-0.219-20.456*$4+0.302*$4**2-0.009*$4**3):(0.01*(-20.456+2*0.302*$4-3*0.009*$4**2)) w e lt 1 notitle, 'datkalt.dat' u (($8+15)*60):(-0.219-20.456*$5+0.302*$5**2-0.009*$5**3):(0.01*(-20.456+2*0.302*$5-3*0.009*$5**2)) w e lt 2 notitle

set output
!epstopdf tempzeitstick.eps


set output 'wiedtempraum.tex'

set xlabel 'Temperatur T [\si{\celsius}]'
set ylabel 'Widerstand R [\si{\ohm}]'

p 'spanraum.dat' u (0.219+20.456*$3-0.302*$3**2+0.009*$3**3):(3*$4/0.5):(1) w e t 'AL', 'spanraum.dat' u (0.219+20.456*$2-0.302*$2**2+0.009*$2**3):($1*3/0.5):(1) w e t 'FE'

set output
!epstopdf wiedtempraum.eps


set output 'wiedtempstick.tex'

p 'spanstick.dat' u (-0.219-20.456*$1+0.302*$1**2-0.009*$1**3):($3/0.5):(1) w e t 'AL', 'spanstick.dat' u (-0.219-20.456*$2+0.302*$2**2-0.009*$2**3):($4/0.5):(1) w e t 'FE'

#p 'datkalt.dat' u (-0.219-20.456*$2+0.302*$2**2-0.009*$2**3):($6/0.5) t 'AL', 'datkalt.dat' u (-0.219-20.456*$3+0.302*$3**2-0.009*$3**3):($7/0.5) t 'FE'

set output
!epstopdf wiedtempstick.eps


set output 'leisttempraum.tex'

set ylabel 'Leistung P [\si{\watt}]'

p 'spanraum.dat' u (0.219+20.456*$3-0.302*$3**2+0.009*$3**3):(3*$4*0.5):(0.25) w e t 'AL', 'spanraum.dat' u (0.219+20.456*$2-0.302*$2**2+0.009*$2**3):($1*3*0.5):(0.25) w e t 'FE'
#p 'datwarm.dat' u (0.219+20.456*$4-0.302*$4**2+0.009*$4**3):(0.5*3*$8) t 'AL', 'datwarm.dat' u (0.219+20.456*$5-0.302*$5**2+0.009*$5**3):(0.5*3*$9) t 'FE'

set output
!epstopdf leisttempraum.eps


set output 'leisttempstick.tex'

p 'spanstick.dat' u (-0.219-20.456*$1+0.302*$1**2-0.009*$1**3):($3*0.5):(0.25) w e t 'AL', 'spanstick.dat' u (-0.219-20.456*$2+0.302*$2**2-0.009*$2**3):($4*0.5):(0.25) w e t 'FE'

#p 'datkalt.dat' u (-0.219-20.456*$2+0.302*$2**2-0.009*$2**3):($6*3/0.5) t 'AL', 'datkalt.dat' u (-0.219-20.456*$3+0.302*$3**2-0.009*$3**3):($7*3/0.5) t 'FE'

set output
!epstopdf leisttempstick.eps

d(x)=3*8.3

set xlabel 'Temperatur T [\si{\kelvin}]'
set ylabel 'c [\si{\joule\per\gram\per\kelvin}]'
set output 'spezal.tex'

p 'spezdatal.dat' u (T(-$2)):(26.982*($3*0.5)/(52.6*(0.0557+165*10**(-5)*(T(-$2)-T(-7.36))))):(sqrt((0.25/(56.2*(0.0557-165*10**(-5)*(T(-$2)-T(-6.68)))) )**2+(0.0003*($2*0.5))/(56.2*(0.0557-165*10**(-5)*(T(-$2)-T(-6.68))))**2+(2*10**(-5)*($2*0.5)/(56.2*(0.0557-165*10**(-5)*(T(-$2)-T(-6.68))**2)))**2)) w e t 'Al', d(x) t 'Dulong-Petit', 'spezdatal.dat' u (T($5)):(26.982*($6*1.5)/(56.2*(0.225+94*10**(-5)*(T($5)-T(0.95))))):(sqrt((26.982*0.25/(56.2*(0.225-94*10**(-5)*(T($5)-T(0.95)))) )**2+(0.005*(26.982*$6*0.5))/(56.2*(0.225-94*10**(-5)*(T($5)-T(0.95))))**2+(2*10**(-5)*(26.982*$6*0.5)/(56.2*(0.225-94*10**(-5)*(T($5)-T(0.95))**2)))**2)) w e notitle lt 1

set output
!epstopdf spezal.eps


set output 'spezfe.tex'

p 'spezdatfe.dat' u (T(-$1)):(($2*0.5)/(148.7*(0.057-178*10**(-5)*(T(-$2)-T(-6.68))))):(sqrt((0.25/(148.7*(0.057-178*10**(-5)*(T(-$2)-T(-6.68)))) )**2+(0.003*($2*0.5))/(148.7*(0.057-178*10**(-5)*(T(-$2)-T(-6.68))))**2+(3*10**(-5)*($2*0.5)/(148.7*(0.057-178*10**(-5)*(T(-$2)-T(-6.68))**2)))**2)) w e t 'Fe', 'spezdatfe.dat' u (T($3)):(($4*1.5)/(148.7*(0.085+2400*10**(-5)*(T($3)-T(0.93))))):(sqrt((0.25/(148.7*(0.085-2400*10**(-5)*(T($3)-T(0.93)))) )**2+(0.002*($4*0.5))/(148.7*(0.058-2400*10**(-5)*(T($3)-T(0.93))))**2+(400*10**(-5)*($4*0.5)/(148.7*(0.058-2400*10**(-5)*(T($3)-T(0.93))**2)))**2)) w e lt 1 notitle#, d(x) t 'Dulong-Petit'

set output
!epstopdf spezfe.eps
